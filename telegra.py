import time
import requests
from bs4 import BeautifulSoup
import telebot
import fitz 
from PIL import Image
from enum import Enum
import re
from datetime import datetime

TOKEN = '6081741605:AAFGrStoRyaAw756zvt15lBXSts3fILTVuA'
CHAT_ID = '@ok9392Timetable'
FILE_NAME = 'file.pdf'
ZOOM_VALUE = 2
IMG_CROP_LEFT_POINT = 61
IMG_CROP_TOP_POINT = 30
IMG_CROP_BOTTOM_POINT =  605
IMG_CROP_RIGHT_POINT = 1624
TIMETABLE_ID = 'rasp'
TIMETABLE_SCREENSHOT_NAME = 'screenshot.jpg'
CROPPED_SCREENSHOT_NAME = 'cropped_screenshot.jpg'

timetable_subtitle = 'Расписание занятий на'
url = 'https://mrk-bsuir.by'
search_text = '0К9392'

subtitle_length = len(timetable_subtitle)


bot = telebot.TeleBot(TOKEN)


timetable_title = ""
prev_timetable_title = ""

class PolandDaysOfWeekList(Enum):
    MONDAY = 'Poniedziałek'
    TUESDAY = 'Wtorek'
    WEDNESDAY = 'Środa'
    THURSDAY = 'Czwartek'
    FRIDAY = 'Piątek',
    SATURDAY = 'Sobota'

class RussianDaysOfWeekList(Enum):
    MONDAY = 'понедельник'
    TUESDAY = 'вторник'
    WEDNESDAY = 'среда'
    THURSDAY = 'четверг'
    FRIDAY = 'пятница',
    SATURDAY = 'суббота'

class MonthsList(Enum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12

class PolandMonthsList(Enum):
    JANUARY = 'Styczeń'
    FEBRUARY = 'Luty'
    MARCH = 'Marzec'
    APRIL = 'Kwiecień'
    MAY = 'Maj'
    JUNE = 'Czerwiec'
    JULY = 'Lipiec'
    AUGUST = 'Sierpień'
    SEPTEMBER = 'Wrzesień'
    OCTOBER = 'Październik'
    NOVEMBER = 'Listopad'
    DECEMBER = 'DECEMBER'



def get_poland_timetable(timetable_text):
    days_map = {
        RussianDaysOfWeekList.MONDAY.value: PolandDaysOfWeekList.MONDAY.value,
        RussianDaysOfWeekList.TUESDAY.value: PolandDaysOfWeekList.TUESDAY.value,
        RussianDaysOfWeekList.WEDNESDAY.value: PolandDaysOfWeekList.WEDNESDAY.value,
        RussianDaysOfWeekList.THURSDAY.value: PolandDaysOfWeekList.THURSDAY.value,
        RussianDaysOfWeekList.FRIDAY.value: PolandDaysOfWeekList.FRIDAY.value,
    }
    for day in days_map:
        if day in timetable_text:
            return days_map[day]
    return PolandDaysOfWeekList.SATURDAY.value

def get_poland_month_name(month_number):
    months_map = {
        MonthsList.JANUARY.value: PolandMonthsList.JANUARY.value,
        MonthsList.FEBRUARY.value: PolandMonthsList.FEBRUARY.value,
        MonthsList.MARCH.value: PolandMonthsList.MARCH.value,
        MonthsList.APRIL.value: PolandMonthsList.APRIL.value,
        MonthsList.MAY.value: PolandMonthsList.MAY.value,
        MonthsList.JUNE.value: PolandMonthsList.JUNE.value,
        MonthsList.JULY.value: PolandMonthsList.JULY.value,
        MonthsList.AUGUST.value: PolandMonthsList.AUGUST.value,
        MonthsList.SEPTEMBER.value: PolandMonthsList.SEPTEMBER.value,
        MonthsList.OCTOBER.value: PolandMonthsList.OCTOBER.value,
        MonthsList.NOVEMBER.value: PolandMonthsList.NOVEMBER.value,
        MonthsList.DECEMBER.value: PolandMonthsList.DECEMBER.value,
    }
    return months_map.get(month_number, PolandMonthsList.DECEMBER.value)


def get_poland_month_result(timetable_day, current_month):
    if timetable_day == 1:
        return get_poland_month_name(current_month + 1)
    else:
        return get_poland_month_name(current_month)


while True:
    try:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        element = soup.find('a', {'id': TIMETABLE_ID})
        timetable_link = element["href"]
        timetable_text = element.text
        text_length = len(timetable_text)

        prev_timetable_title = timetable_title
        timetable_title = timetable_text[subtitle_length:text_length]

        if prev_timetable_title != timetable_title :
            pdf = requests.get(timetable_link)
            with open(FILE_NAME, 'wb') as current_file:
                current_file.write(pdf.content)
            document = fitz.open(FILE_NAME)
            for page in document:
                text = page.get_text()
                if search_text in text: 
                    mat = fitz.Matrix(ZOOM_VALUE, ZOOM_VALUE)
                    pix = page.get_pixmap(matrix = mat)
                    pix.save(TIMETABLE_SCREENSHOT_NAME)
                    break
            img = Image.open(TIMETABLE_SCREENSHOT_NAME)

            img_copy = img.copy()

            img_crop = img_copy.crop((
                IMG_CROP_LEFT_POINT,
                IMG_CROP_TOP_POINT,
                IMG_CROP_RIGHT_POINT,
                IMG_CROP_BOTTOM_POINT,
            ))

        
            img_crop.save(CROPPED_SCREENSHOT_NAME)
        
            with open(FILE_NAME, 'rb') as pdfFile, open(CROPPED_SCREENSHOT_NAME, 'rb') as currentFile:
                poland_title = get_poland_timetable(timetable_text)
                timetable_day = re.findall(r'\d+', timetable_title)[0]
                currentMonth = datetime.now().month
                poland_month_name = get_poland_month_result(timetable_day, currentMonth)
                print(poland_month_name)
                title_result = timetable_title + '\n' + poland_title + ", " + timetable_day + " " + poland_month_name
                
               
                bot.send_photo(chat_id='@ok9392Timetable', photo=currentFile, caption=title_result)
    except Exception as e:
        print(e)
    time.sleep(600) 
